<?php

/**
 * @file
 * Install, update and uninstall functions for agoradesign installation profile.
 */

use Drupal\user\Entity\User;
use Drupal\user\RoleInterface;
use Drupal\user\UserInterface;

/**
 * Implements hook_install().
 */
function agoradesign_install() {
  // By default, only allow administrators to create user accounts.
  $user_settings = \Drupal::configFactory()->getEditable('user.settings');
  $user_settings->set('register', UserInterface::REGISTER_ADMINISTRATORS_ONLY)->save(TRUE);

  // Assign user 1 the "administrator" role.
  $user = User::load(1);
  $user->roles[] = 'administrator';
  $user->save();

  // Allow authenticated users to use shortcuts.
  user_role_grant_permissions(RoleInterface::AUTHENTICATED_ID, ['access shortcuts']);

  // Populate the default shortcut set.
  $shortcut_storage = \Drupal::entityTypeManager()->getStorage('shortcut');
  $shortcut = $shortcut_storage->create([
    'shortcut_set' => 'default',
    'title' => t('Add content'),
    'weight' => -20,
    'link' => ['uri' => 'internal:/node/add'],
  ]);
  $shortcut->save();

  $shortcut = $shortcut_storage->create([
    'shortcut_set' => 'default',
    'title' => t('All content'),
    'weight' => -19,
    'link' => ['uri' => 'internal:/admin/content'],
  ]);
  $shortcut->save();

  $shortcut = $shortcut_storage->create([
    'shortcut_set' => 'default',
    'title' => t('Content types'),
    'weight' => -17,
    'link' => ['uri' => 'internal:/admin/structure/types'],
  ]);
  $shortcut->save();

  $shortcut = $shortcut_storage->create([
    'shortcut_set' => 'default',
    'title' => t('Views'),
    'weight' => -15,
    'link' => ['uri' => 'internal:/admin/structure/views'],
  ]);
  $shortcut->save();

  $shortcut = $shortcut_storage->create([
    'shortcut_set' => 'default',
    'title' => t('Block layout'),
    'weight' => -14,
    'link' => ['uri' => 'internal:/admin/structure/block'],
  ]);
  $shortcut->save();

  $shortcut = $shortcut_storage->create([
    'shortcut_set' => 'default',
    'title' => t('Menus'),
    'weight' => -13,
    'link' => ['uri' => 'internal:/admin/structure/menu'],
  ]);
  $shortcut->save();

  $shortcut = $shortcut_storage->create([
    'shortcut_set' => 'default',
    'title' => t('Taxonomy'),
    'weight' => -12,
    'link' => ['uri' => 'internal:/admin/structure/taxonomy'],
  ]);
  $shortcut->save();

  $shortcut = $shortcut_storage->create([
    'shortcut_set' => 'default',
    'title' => t('Image styles'),
    'weight' => 15,
    'link' => ['uri' => 'internal:/admin/config/media/image-styles'],
  ]);
  $shortcut->save();

  $shortcut = $shortcut_storage->create([
    'shortcut_set' => 'default',
    'title' => t('User interface translation'),
    'weight' => 16,
    'link' => ['uri' => 'internal:/admin/config/regional/translate'],
  ]);
  $shortcut->save();

  $shortcut = $shortcut_storage->create([
    'shortcut_set' => 'default',
    'title' => t('Content language'),
    'weight' => 17,
    'link' => ['uri' => 'internal:/admin/config/regional/content-language'],
  ]);
  $shortcut->save();

  $shortcut = $shortcut_storage->create([
    'shortcut_set' => 'default',
    'title' => t('Single export'),
    'weight' => 18,
    'link' => ['uri' => 'internal:/admin/config/development/configuration/single/export'],
  ]);
  $shortcut->save();

  $shortcut = $shortcut_storage->create([
    'shortcut_set' => 'default',
    'title' => t('Performance'),
    'weight' => 19,
    'link' => ['uri' => 'internal:/admin/config/development/performance'],
  ]);
  $shortcut->save();

  $shortcut = $shortcut_storage->create([
    'shortcut_set' => 'default',
    'title' => t('Cron'),
    'weight' => 20,
    'link' => ['uri' => 'internal:/admin/config/system/cron'],
  ]);
  $shortcut->save();

  // Enable the admin theme.
  \Drupal::configFactory()->getEditable('node.settings')
    ->set('use_admin_theme', TRUE)
    ->save(TRUE);

  // Disable 'Users may set their own time zone' and set week begin to Monday.
  \Drupal::configFactory()->getEditable('system.date')
    ->set('first_day', 1)
    ->set('timezone.user.configurable', FALSE)
    ->save(TRUE);

  // We do not want to have a hard dependency on entity_usage, but install it,
  // if we have loaded the module into the file system.
  /** @var \Drupal\Core\Extension\ModuleExtensionList $module_extension_list */
  $module_extension_list = \Drupal::service('extension.list.module');
  $module_data = $module_extension_list->reset()->getList();
  if (isset($module_data['entity_usage']) && empty($module_data['entity_usage']->status)) {
    /** @var \Drupal\Core\Extension\ModuleInstallerInterface $module_installer */
    $module_installer = \Drupal::service('module_installer');
    $module_installer->install(['entity_usage']);
  }

  // Do not send e-mail notifications for updates.
  \Drupal::configFactory()->getEditable('update.settings')->set('notification.emails', [])->save();
}
