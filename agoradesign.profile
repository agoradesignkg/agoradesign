<?php

/**
 * @file
 * Enables modules and site configuration for a agoradesign site installation.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 *
 * Please note that we have not used UI installation for the past years. The
 * recommended way is to use Drush instead!
 */
function agoradesign_form_install_configure_form_alter(&$form, FormStateInterface $form_state) {
  // Add a placeholder as example that one can choose an arbitrary site name.
  $form['site_information']['site_name']['#attributes']['placeholder'] = t('My site');

  // Pre-populate the site email.
  $form['site_information']['site_mail']['#default_value'] = 'office@agoradesign.at';

  // Account information defaults.
  $form['admin_account']['account']['name']['#default_value'] = 'superadmin';
  $form['admin_account']['account']['mail']['#default_value'] = 'office@agoradesign.at';

  // Date/time settings.
  $form['regional_settings']['site_default_country']['#default_value'] = 'AT';
  $form['regional_settings']['date_default_timezone']['#default_value'] = 'Europe/Vienna';

  // Unset the timezone detect stuff.
  unset($form['regional_settings']['date_default_timezone']['#attributes']['class']);

  // Only check for updates, no need for email notifications.
  $form['update_notifications']['update_status_module']['#default_value'] = [1];
}
